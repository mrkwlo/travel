$(document).ready(function(){
  $("form#regForm").submit(function() { //regForm is submitted
    var firstname = $("#firstname").val(); 
    var lastname = $("#lastname").val(); 
    var select = document.getElementById('classroom');
    var classroom = select.options[select.selectedIndex].getAttribute('value');
    var inputDate = document.getElementById('datepicker');
    var datepicker = inputDate.value;
    var date = new Date(inputDate.value);
    var year = date.getFullYear();
    var dd   = date.getDate();
    var mm   = date.getMonth();
    var cdn  =  true;
    //alert( "firstname=" + firstname + "&lastname=" + lastname + "&classroom="+ classroom +"&datepicker="+datepicker);
    if((mm+1)!=1)     {alert ("Month out of range"); cdn = false;}
    else if (year != 2015) {alert ("Year out of range");  cdn = false;}
    else if (dd > 15)      {alert ("Day out of range");   cdn = false;};
    if ( cdn && firstname && lastname && classroom && datepicker) { // values are not empty
    $.ajax({
        type: "GET",
        url: "/cgi-bin/reg.pl", // URL of the Perl script
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        // send firstname and lastname as parameters to the Perl script
        data: "firstname=" + firstname + "&lastname=" + lastname + "&classroom="+ classroom +"&datepicker="+datepicker,
        // script call was *not* successful
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          $('div#regResult').text("responseText: " + XMLHttpRequest.responseText 
            + ", textStatus: " + textStatus 
            + ", errorThrown: " + errorThrown);
          $('div#regResult').addClass("error");
        }, // error 
        // script call was successful 
        // data contains the JSON values returned by the Perl script 
        success: function(data){
          if (data.error) { // script returned error
            $('div#regResult').text("data.error: " + data.error);
            $('div#regResult').addClass("error");
          } // if
          else { // login was successful
            //alert("Your registration Id:"+data.regid);
            $('#regForm').hide();
            $('div#regResult').text("data.success: " + data.success 
              + ", data.regid: " + data.regid);
            $('div#regResult').addClass("success");
          } //else
        } // success
      }); // ajax
    } // if
    else {
      $('div#regResult').text("enter firstname and lastname and proper date");
      $('div#regResult').addClass("error");
    } // else
    $('div#regResult').fadeIn();
    return false;
  });
});
