#!/usr/bin/perl -w

# DBI is the standard database interface for Perl
# DBD is the Perl module that we use to connect to the <a href="http://mysql.com/" />MySQL</a> database
use DBI;
use DBD::mysql;

# we use CGI since this will be executed in a browser
use CGI qw(:standard);

use warnings;

#----------------------------------------------------------------------
# open the accessDB file to retrieve the database name, host name, user name and password

# assign the values in the accessDB file to the variables
my $database = "mydb" ;
my $host = "localhost";
my $userid = "root";
my $passwd = "ala11";
my $port = "3306";

# the chomp() function will remove any newline character from the end of a string
chomp ($database, $host, $userid, $passwd);

#----------------------------------------------------------------------

# print the header
print header;

# HTML for the beginning of the table
# we are putting a border around the table for effect
print "<table border=\"1\" width=\"800\"> \n";

# print your table column headers
print "<tr><td>IdReg</td><td>IdPer</td><td>Name</td><td>Room</td><td>DateofReg</td></tr>\n";

# invoke the ConnectToMySQL sub-routine to make the database connection
$connection = ConnectToMySql($database);

# set the value of your SQL query
$query = "select r.IdRejestracja, o.IdOsoba, o.Imie, r.IdKlasa, r.DataRej from Rejestracja r, Osoba o Where r.IdOsoba=o.IdOsoba order by 1 desc";

# prepare your statement for connecting to the database
$statement = $connection->prepare($query);

# execute your SQL statement
$statement->execute();

# retrieve the values returned from executing your SQL statement
while (@data = $statement->fetchrow_array()) {
$fld1 = $data[0];
$fld2 = $data[1];
$fld3 = $data[2];
$fld4 = $data[3];
$fld5 = $data[4];

# print your table rows
print "<tr><td>$fld1</td><td>$fld2</td><td>$fld3</td>
<td>$fld4</td> <td>$fld5</td> </tr>\n";
#
}

# close your table
print "</table>\n";

# exit the script
exit;

#--- start sub-routine ------------------------------------------------
sub ConnectToMySql {
#----------------------------------------------------------------------

my ($db) = @_;

# assign the values to your connection variable
my $connectionInfo="dbi:mysql:database=$db;host=$host;port=$port";

# make connection to database
my $l_connection = DBI->connect($connectionInfo,$userid,$passwd);

# the value of this connection is returned by the sub-routine
return $l_connection;

}
