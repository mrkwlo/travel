#!/usr/bin/perl -T
use CGI;
use DBI;
use strict;
use warnings;


# read the CGI params
my $cgi = CGI->new;
my $firstname = $cgi->param("firstname");
my $lastname  = $cgi->param("lastname");
my $classroom = $cgi->param("classroom");
my $datepicker= $cgi->param("datepicker");


# connect to the database
my $dbh = DBI->connect("DBI:mysql:database=mydb;host=localhost;port=3306",  
  "root", "ala11") 
  or die $DBI::errstr;

# -- getting OsobaID --
# check the firstname and lastname in the database
my $statement = qq{SELECT IdOsoba FROM Osoba WHERE Imie=? and Nazwisko=?};
my $sth = $dbh->prepare($statement) or die $dbh->errstr;
$sth->execute($firstname, $lastname) or die $sth->errstr;

my ($osobaID) = $sth->fetchrow_array;

if ($osobaID) {
  # do stuff for one row returned
  # for debug purpose
} else {
  # do stuff for no rows returned - insert new Osoba into Osoba
  my $sttm_max = qq{SELECT max(IdOsoba) FROM Osoba};
  my $sth4 = $dbh->prepare($sttm_max) or die $dbh->errstr;

  $sth4->execute() or die $sth4->errstr;
  my ($maxOsoID) = $sth4->fetchrow_array;

  if ($maxOsoID) {
    # do stuff for one row returned
    $maxOsoID++;
    } else {
    # do stuff for no rows returned
    $maxOsoID=1;
    }

  my $sql_ins_os = "INSERT INTO Osoba VALUES( ?,?,? )" ;
  my $sth1 = $dbh->prepare($sql_ins_os) or die $dbh->errstr;
  $sth1->execute($firstname , $lastname, $maxOsoID ) or die $sth1->errstr;

  $osobaID = $maxOsoID;
}

# max from Rejestracja
my $rej = qq{SELECT max(IdRejestracja) FROM Rejestracja};
my $sth2 = $dbh->prepare($rej) or die $dbh->errstr;
$sth2->execute() or die $sth2->errstr;

my ($maxRegID) = $sth2->fetchrow_array;

if ($maxRegID) {
  # do stuff for one row returned
  $maxRegID++;
} else {
  # do stuff for no rows returned
  $maxRegID=1;
}

# insert into Rejestracja 
my $sql_rej = "INSERT INTO Rejestracja  VALUES(?, ?, ?, ?)" ;
my $sth3 = $dbh->prepare($sql_rej) or die $dbh->errstr;

$sth3->execute($maxRegID, $osobaID, $classroom, $datepicker) or die $sth3->errstr;

#$dbh->disconnect or warn "Disconnection failed: $DBI::errstr\n";

my $json = ($maxRegID) ? 
  qq{{"success" : "You are registered successful", "regid" : "$maxRegID"}} 
:
  qq{{"error" : "Sorry, you are not registered"}} ;

print $cgi->header(-type => "application/json", -charset => "utf-8") ;# return JSON string
#print $cgi->header(-type => "application/json", -charset => "utf-8");
print $json;
#print "\nPersonID: ";
#print $osobaID;
#print "\nMaxRegId : ";
#print $maxRegID;

